#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
# create and add ec2-user to docker group
sudo usermod -aG docker ec2-user


