variable "prefix" {
  default = "raad" //  Recipe App Api Devops
}

variable "project" {
  default = "Recipe App"
}

variable "contact" {
  default = "email@me.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default     = "recipe-app-api-devops-bastion-id_gitlab"
  description = "SSH keypair name to access Bastion instance"
}

variable "ecr_imape_api" {
  description = "ECR image for API"
  default     = "569363278156.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "569363278156.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "yet-another-jubilee.info"
}

variable "subdomain" {
  description = "Subdomain per enviroment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}